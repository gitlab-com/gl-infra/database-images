image_os                = "ubuntu-2004"
image_description       = "Gitlab Ubuntu 20.04 Base Image"
zone                    = "us-east1-b"
image_storage_locations = ["us-east1"]
image_labels = {
  "image_type" = "database-image-base"
  "team"       = "reliability"
}
labels = {
  "app"  = "database"
  "team" = "reliability"
}
