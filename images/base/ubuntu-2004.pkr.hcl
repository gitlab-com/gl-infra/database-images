locals {
  buildtime = formatdate("YYYYMMDDhhmmss", timestamp())
}
source "googlecompute" "gitlab_ubuntu" {
  project_id              = "${var.project_name}"
  source_image_family     = "${var.image_os}-lts"
  ssh_username            = "packer"
  zone                    = "${var.zone}"
  image_name              = "gitlab-${var.image_os}-${var.image_type}-${local.buildtime}"
  image_description       = "${var.project_name}"
  image_family            = "gitlab-${var.image_os}-lts"
  image_storage_locations = "${var.image_storage_locations}"
  image_labels            = "${var.image_labels}"
}

build {
  sources = ["sources.googlecompute.gitlab_ubuntu"]

  provisioner "shell" {
    environment_vars = [
      "ANSIBLE_VERSION=${var.ansible_version}",
    ]
    script = "bin/ansible.sh"
  }

  provisioner "ansible-local" {
    staging_directory = "/home/packer/ansible"
    group_vars        = "ansible/group_vars/"
    playbook_dir      = "${var.playbooks_path}/${var.image_type}"
    playbook_file     = "${var.playbooks_path}/${var.image_type}/main.yml"
    galaxy_file       = "${var.playbooks_path}/${var.image_type}/requirements.yml"
    galaxy_command    = "ansible-galaxy collection install -r"
    extra_arguments = [
      "--extra-vars", "ANSIBLE_COLLECTIONS_PATH=/home/packer/ansible/roles/ansible_collections"
    ]
  }
}
