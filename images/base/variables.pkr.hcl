variable "project_name" {
  type    = string
  default = "gitlab-db-benchmarking"
}

variable "image_os" {
  type = string
}

variable "image_type" {
  type    = string
  default = "base"
}

variable "image_description" {
  type = string
}

variable "zone" {
  type = string
}

variable "image_storage_locations" {
  type = list(string)
}

variable "image_labels" {
  type = map(string)
}

variable "labels" {
  type = map(string)
}

variable "ansible_version" {
  type    = string
  default = "4.8.0"
}

variable "playbooks_path" {
  type    = string
  default = "ansible/playbooks"
}