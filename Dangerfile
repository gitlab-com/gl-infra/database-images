# frozen_string_literal: true

RELEASE_DAY = 22.freeze

MISSING_DESCRIPTION_MESSAGE = <<~END_OF_MESSAGE
  Please provide a merge request description.
END_OF_MESSAGE

SKIPPING_VALIDATION_MESSAGE = <<~END_OF_MESSAGE
  Skipping validation of merge request description because of ~backstage label.
END_OF_MESSAGE

MISSING_ISSUE_LINK_MESSAGE = <<~END_OF_MESSAGE
  **Please provide an issue link in the merge request description to the gitlab.com issue related to this change.**

  For version bumps, typos and other small changes, or in time-limited situations (eg, during an incident),
  add the label ~backstage to exempt this change from this requirement.
END_OF_MESSAGE

SKIPPING_ENV_ISOLATION_MESSAGE = <<~END_OF_MESSAGE
  Skipping validation of env isolation because of because of ~backstage label.
END_OF_MESSAGE

ENV_ISOLATION_MESSAGE = <<~END_OF_MESSAGE
  Detected changes for both gprd and gstg, please make separate MRs.

  In case of emergency, you can add the ~backstage label to skip this check.
END_OF_MESSAGE

LIST_OF_AFFECTED_HOSTS_MESSAGE = <<~END_OF_MESSAGE
  The following **%{count}** host(s) will be affected by the changes made in this MR:
  %{hosts}
END_OF_MESSAGE

RELEASE_DAY_MESSAGE = <<~END_OF_MESSAGE
  This change is being evaluated on the **%{release_day}**nd which is [release day](https://gitlab.com/gitlab-org/release/docs/blob/master/general/monthly/process.md).


  **Please carefully evaluate the need for the change**:

  * [maintenance changes](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#changes) should not be done on release day
  * Service changes should be evaluated for criticality, please consider postponing unless the change is urgent
END_OF_MESSAGE

GSTG_PATHS = ['roles/gstg-', 'environments/gstg', 'environments/stg'].freeze
GPRD_PATHS = ['roles/gprd-', 'roles/gitlab-', 'environments/gprd', 'environments/prd'].freeze

def validate_mr
  # The cookbook-publisher user is automatically creating MR's that are backstage in nature, so skip those MR's.
  return if gitlab.mr_author == 'cookbook-publisher'

  validate_mr_description
  validate_mr_env_isolation
end

def validate_mr_description
  fail MISSING_DESCRIPTION_MESSAGE if gitlab.mr_body.empty?

  # `backstage` label describes smaller changes like fixing typos and docs, we can safely skip those MR's.
  if gitlab.mr_labels.include? 'backstage'
    message SKIPPING_VALIDATION_MESSAGE
    return
  end

  fail MISSING_ISSUE_LINK_MESSAGE unless gitlab.mr_body.match?(%r{https://gitlab.com/[\w+-/]+/issues/\d+})
end

def validate_mr_env_isolation
  if gitlab.mr_labels.include? 'backstage'
    message SKIPPING_ENV_ISOLATION_MESSAGE
    return
  end

  has_gstg_changes = (git.added_files + git.modified_files + git.deleted_files)
    .find { |file| GSTG_PATHS.any? { |prefix| file.start_with?(prefix) } }

  has_gprd_changes = (git.added_files + git.modified_files + git.deleted_files)
    .find { |file| GPRD_PATHS.any? { |prefix| file.start_with?(prefix) } }

  fail ENV_ISOLATION_MESSAGE if has_gstg_changes && has_gprd_changes
end

def print_affected_hosts
  roles = (git.added_files + git.modified_files + git.deleted_files).lazy
    .select { |file| file.start_with?('roles/') && file.end_with?('.json') }
    .map { |file| file.gsub(%r{^roles/(.*)\.json}, '\1') }
    .map { |role| "roles:#{role}" }
    .force

  fqdns = roles.each_slice(10).map do |roles_slice|
    result = `knife search --id-only '#{roles_slice.join(' ')}'`
    result.split
  end

  fqdns.flatten!
  fqdns.sort!
  fqdns.uniq!

  return if fqdns.empty?

  hosts_list = if fqdns.length <= 25
                fqdns.join("\n")
               else
                 list = fqdns.join("<br/>")
                "<details>#{list}</details>"
               end

  message(LIST_OF_AFFECTED_HOSTS_MESSAGE % {count: fqdns.length, hosts: hosts_list})
end

def check_for_release_date
  return unless Time.new.day == RELEASE_DAY
  message(RELEASE_DAY_MESSAGE % {release_day: RELEASE_DAY})
end

print_affected_hosts
check_for_release_date
validate_mr
