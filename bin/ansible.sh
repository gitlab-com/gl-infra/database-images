#!/bin/bash -e
sudo apt -y update && sudo apt-get -y upgrade
sudo apt-get clean
grep -E -v '^#|^ *$' /etc/apt/sources.list
sudo apt -y install software-properties-common build-essential

echo "Installing Pip and Ansible"
sudo apt -y install python3.8-venv python3-pip
sudo python3 -m pip install ansible=="$ANSIBLE_VERSION"

echo "Installing Consul"
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install consul
